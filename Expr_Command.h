//==============================================================================
/**
 * Honor Pledge:
 *
 * I pledge that I have neither given nor received any help
 * on this assignment.
 */
//==============================================================================
#ifndef _EXPR_COMMAND_H_
#define _EXPR_COMMAND_H_

#include "Stack.h"

class Expr_Command
{
public:
	Expr_Command(Stack<int> & s)
	: s_(s),
  	  precedence_val_(0) 
	{ }
	
	virtual ~Expr_Command(void){ }

	virtual int get_precedence_val(void) = 0;
	virtual void execute(void) = 0;
protected:
	Stack <int> & s_;
	int precedence_val_;
};

#endif