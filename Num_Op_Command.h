//==============================================================================
/**
 * Honor Pledge:
 *
 * I pledge that I have neither given nor received any help
 * on this assignment.
 */
//==============================================================================
#ifndef _NUM_OP_COMMAND_H_
#define _NUM_OP_COMMAND_H_

#include "Expr_Command.h"

// COMMENT: Do not place implementation code in the header file. Instead,
// create a separate source file.
// FIX: Attempted to separate, but when I do, I receive errors regarding
// my Stack and Array template classes.
class Num_Op_Command : public Expr_Command
{
public:
	Num_Op_Command(Stack <int> & s, int num)
	: Expr_Command(s),
	  num_(num)
	{ 
		precedence_val_ = 0;
	}

	~Num_Op_Command(void) { }

	void execute(void)
	{
		s_.push(this->num_);
	}

	int get_precedence_val(void)
	{
		return precedence_val_;
	}
private:
	int num_;
};

#endif