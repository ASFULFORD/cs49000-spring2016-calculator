//==============================================================================
/**
 * Honor Pledge:
 *
 * I pledge that I have neither given nor received any help
 * on this assignment.
 */
//==============================================================================
#ifndef _CALCULATOR_H_
#define _CALCULATOR_H_

#include <string>
#include "Array.h"
#include "Stack.h"
#include "Expr_Command.h"
#include "Stack_Expr_Command_Factory.h"

class Calculator
{
public:
	Calculator(void);
	~Calculator(void);

	int get_result(void);
	void set_infix(std::string str);
	std::string get_infix(void);
	int evaluate_postfix(void);
	int string_to_int(std::string str);
	void infix_to_postfix(void);

private:
	int res_;
	int num_of_expr_;
	std::string infix_;
	Array <Expr_Command *> postfix_;
	Stack <int> result_;
	Stack_Expr_Command_Factory factory_;
};

#endif