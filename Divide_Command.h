//==============================================================================
/**
 * Honor Pledge:
 *
 * I pledge that I have neither given nor received any help
 * on this assignment.
 */
//==============================================================================
#ifndef _DIVIDE_COMMAND_H_
#define _DIVIDE_COMMAND_H_
 
#include "Binary_Op_Command.h"
#include <stdexcept>

// COMMENT: Do not place implementation code in the header file. Instead,
// create a separate source file.
// FIX: Attempted to separate, but when I do, I receive errors regarding
// my Stack and Array template classes.
class Divide_Command : public Binary_Op_Command
{
public:
	Divide_Command(Stack<int> & s)
	: Binary_Op_Command(s)
	{ 
		this->precedence_val_ = 2;
	}

	~Divide_Command(void) { }

	int get_precedence_val(void)
	{
		return precedence_val_;
	}

	int evaluate(int n1, int n2)
	{
		if (n2 == 0)
			throw std::overflow_error("Divide by zero exception");
    	// COMMENT: Your code will crash if you have divide by 0.
		// FIX:     Added exception above.
		return n1 / n2;
	}
};

#endif