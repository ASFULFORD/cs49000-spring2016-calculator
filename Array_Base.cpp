//==============================================================================
/**
 * Honor Pledge:
 *
 * I pledge that I have neither given nor received any help
 * on this assignment.
 */
//==============================================================================
#include <stdexcept>         // for std::out_of_bounds exception
#define DEFAULT_SIZE 10
//
// Array_Base
//
template <typename T>
Array_Base <T>::Array_Base (void)	
: data_(new T[DEFAULT_SIZE]), 
  cur_size_(DEFAULT_SIZE) 
  { }

//
// Array_Base (size_t)
//
template <typename T>
Array_Base <T>::Array_Base (size_t length)
: data_(new T[length]), 
  cur_size_(length) 
  { }

//
// Array_Base (size_t, char)
//
template <typename T>
Array_Base <T>::Array_Base (size_t length, T fill)
: data_(new T[length]), 
  cur_size_(length) 
{
	this->fill(fill);
}

//
// Array_Base (const Array_Base &)
//
template <typename T>
Array_Base <T>::Array_Base (const Array_Base & array)
: data_(new T[array.cur_size_]), 
  cur_size_(array.cur_size_) 
{
	for(size_t i = 0; i < array.cur_size_; ++i)
		this->data_[i] = array.data_[i];
}

//
// ~Array_Base
//
template <typename T>
Array_Base <T>::~Array_Base (void)
{
	if(data_)
		{
			delete[] data_;
			data_ = NULL;
		}
}

//
// operator []
//
template <typename T>
T & Array_Base <T>::operator [] (size_t index)
{
	if(index >= this->cur_size_)
		throw std::out_of_range("Invalid Index");
	return data_[index];
}

//
// operator [] 
//
template <typename T>
const T & Array_Base <T>::operator [] (size_t index) const
{
	if(index >= this->cur_size_)
		throw std::out_of_range("Invalid Index");
	return data_[index];
}	
//
// get
//
template <typename T>
T Array_Base <T>::get (size_t index) const
{
	if(index >= this->cur_size_)
		throw std::out_of_range("Invalid Index");
	return data_[index];
}

//
// set
//
template <typename T>
void Array_Base <T>::set (size_t index, T value)
{
	if(index >= this->cur_size_)
		throw std::out_of_range("Invalid Index");
	this->data_[index] = value;
}

//
// find (char)
//
template  <typename T>
int Array_Base <T>::find (T value) const
{
	this->find(value, 0);
}

//
// find (char, size_t) 
//
template <typename T>
int Array_Base <T>::find (T val, size_t start) const
{
	if(start > this->cur_size_)
		throw std::out_of_range("Invalid Index");
	else
	{
		for(size_t i = start; i < this->cur_size_; ++i)
		{
			if(this->data_[i] == val)
			return i;
		}
		return -1;
	}
}

//
// operator ==
//
template <typename T>
bool Array_Base <T>::operator == (const Array_Base & rhs) const
{
	if(this->cur_size_ != rhs.cur_size_)
		return false;
	else
	{
		for(size_t i = 0; i < this->cur_size_; ++i)
		{
			if(this->data_[i] != rhs.data_[i])
				return false;
		}
		return true;
	}
}

//
// operator !=
//
template <typename T>
bool Array_Base <T>::operator != (const Array_Base & rhs) const
{
	return !(*this == rhs);
}

//
// fill
//
template <typename T>
void Array_Base <T>::fill (T value)
{
	for(size_t i = 0; i < this->cur_size_; ++i)
		this->data_[i] = value;
}