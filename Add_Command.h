//==============================================================================
/**
 * Honor Pledge:
 *
 * I pledge that I have neither given nor received any help
 * on this assignment.
 */
//==============================================================================
#ifndef _ADD_COMMAND_H_
#define _ADD_COMMAND_H_
 
#include "Binary_Op_Command.h"

// COMMENT: Do not place implementation code in the header file. Instead,
// create a separate source file.
// FIX: Attempted to separate, but when I do, I receive errors regarding
// my Stack and Array template classes.
class Add_Command : public Binary_Op_Command
{
public:
	Add_Command(Stack<int> & s)
	: Binary_Op_Command(s)
	{ 
		this->precedence_val_ = 1;
	}

	~Add_Command(void) { }

	int get_precedence_val(void)
	{
		return precedence_val_;
	}

	int evaluate(int n1, int n2)
	{
		return n2 + n1;
	}
};


#endif