//==============================================================================
/**
 * Honor Pledge:
 *
 * I pledge that I have neither given nor received any help
 * on this assignment.
 */
//==============================================================================

#include <string>
#include <sstream>
#include <iostream>
#include "Calculator.h"

int main(int argc, char *argv[])
{
	Calculator calculator;
	int result;
	std::string input;
	std::cout << "Welcome to Calculator 1.0" << std::endl;
	std::cout << "Enter expression: ";
	while(input != "QUIT")
	{
		//Grab input from user
		std::getline(std::cin, input);
		calculator.set_infix(input);

		if(input == "QUIT")
			return 1;

    // COMMENT: You have a memory leak. You need to make sure you
    // delete each command after evaluating the expression.

		//infix to postfix
		calculator.infix_to_postfix();
		
		//evalute expression
		result = calculator.evaluate_postfix();

		std::cout << input << " = " << result << std::endl;

		std::cout << "Enter expression: ";
	}
	return 0;
}