// $Id: Stack.cpp 827 2011-02-07 14:20:53Z hillj $

//==============================================================================
/**
 * Honor Pledge:
 *
 * I pledge that I have neither given nor received any help
 * on this assignment.
 */
//==============================================================================

//
// Stack
//
// Default Constructor. Top_ is -1 representing an empty stack.
template <typename T>
Stack <T>::Stack (void)
: stack_array_(),
  top_(-1)
{ }

//
// Stack
//
// Copy Constructor. Deep copy.
// *** HELP ***
// Whenever I try to access the Array's data_ by using [].  I get an error
// saying that data_ is private.  However, the 'T get (size_t index) const;'
// method works just fine.  Also, the implementation of get() and [] are
// exactly the same.
template <typename T>
Stack <T>::Stack (const Stack & stack)
: stack_array_(stack.stack_array_),
  top_(stack.top_)
{ }

//
// ~Stack
//
// Deconstructor.  Empty since I don't have any pointers or use the keyword
// new.
template <typename T>
Stack <T>::~Stack (void) { }

//
// push
//
// Use an if statement to check whether or not the top of the stack is in bounds.
// If the stack is full, resize the Array then push.
template <typename T>
void Stack <T>::push (T element)
{
	if(this->top_ < this->stack_array_.template Array_Base<T>::size() - 1 || this->top_ == -1)
	{
		this->top_++;
		this->stack_array_.template Array_Base<T>::set(this->top_, element);
	}
	else
	{
		this->stack_array_.template Array<T>::resize(this->stack_array_.template Array<T>::max_size() * 2);
		this->top_++;
		this->stack_array_.template Array_Base<T>::set(top_, element);
	}
}

//
// pop
//
// If stack is empty, throw an exception.  If not, simply decrement the index
// pointing to the top of stack.
template <typename T>
void Stack <T>::pop (void)
{
	if(this->top_ == -1)
		throw empty_exception();
	this->top_--;
}

//
// operator =
//
// Same implementation of the copy constructor.  Deep copy.
template <typename T>
const Stack <T> & Stack <T>::operator = (const Stack & rhs)
{
	// COMMENT Always check for self-assignment first before continuing.
	// *** FIX *** Check for self-assignment first.
	if(this == &rhs)
		return *this;

	this->top_ = rhs.template Array_Base<T>::size();

	if(rhs.template Array_Base<T>::size() > 0)
	{
		for(size_t i = 0; i < rhs.template Array_Base<T>::size(); ++i)
			this->stack_array_.set(i, rhs.stack_array.get(i));
	}
	return *this;
}

//
// clear
//
// Simply set the index to the top of the stack to -1.
// This represents an empty stack.
template <typename T>
void Stack <T>::clear (void)
{
	this->top_ = -1;
}
