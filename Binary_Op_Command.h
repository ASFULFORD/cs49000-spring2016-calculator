//==============================================================================
/**
 * Honor Pledge:
 *
 * I pledge that I have neither given nor received any help
 * on this assignment.
 */
//==============================================================================
#ifndef _BINARY_OP_COMMAND_H_
#define _BINARY_OP_COMMAND_H_

#include "Expr_Command.h"

// COMMENT: Do not place implementation code in the header file. Instead,
// create a separate source file.
// FIX: Attempted to separate, but when I do, I receive errors regarding
// my Stack and Array template classes.
class Binary_Op_Command : public Expr_Command
{
public:
	Binary_Op_Command(Stack<int> & s)
	: Expr_Command(s)
	{ }

	virtual ~Binary_Op_Command(void) { }

	void execute(void)
	{
		int n2 = s_.top();
		s_.pop();
		int n1 = s_.top();
		s_.pop();
		int result = this->evaluate(n1, n2);
		s_.push(result);
	}

	virtual int get_precedence_val(void) = 0;
	virtual int evaluate(int n1, int n2) = 0;
};

#endif