// $Id: Array.cpp 827 2011-02-07 14:20:53Z hillj $

//==============================================================================
/**
 * Honor Pledge:
 *
 * I pledge that I have neither given nor received any help
 * on this assignment.
 */
//==============================================================================
#include <stdexcept>         // for std::out_of_bounds exception
#define DEFAULT_SIZE 10
//
// Default Constructor
//
template <typename T>
Array <T>::Array (void)	
: max_size_(DEFAULT_SIZE),
  Array_Base <T>()
{ }

//
// Length Constructor
//
template <typename T>
Array <T>::Array (size_t length)
: max_size_(length),
  Array_Base <T>(length)
{ }

//
// Fill Constructor
//
template <typename T>
Array <T>::Array (size_t length, T fill)
: max_size_(length),
  Array_Base <T>(length, fill)
{ }

//
// Copy Constructor
//
template <typename T>
Array <T>::Array (const Array & array)
: max_size_(array.max_size_),
  Array_Base <T>(array.max_size_)
{
	*this = array;
}

//
// ~Array
//
template <typename T>
Array <T>::~Array (void)
{ }

//
// operator =
//
template <typename T>
const Array <T> & Array <T>::operator = (const Array & rhs)
{
	if(this == &rhs)
		return *this;

	this->Array_Base<T>::cur_size_ = rhs.size();
	this->max_size_ = rhs.max_size_;
	
	for(size_t i = 0; i < rhs.size(); ++i)
		this->Array_Base<T>::set(i, rhs.get(i));
	return *this;
}

//
// resize
//
template <typename T>
void Array <T>::resize (size_t new_size)
{
	if(new_size <= this->max_size_)
		this->Array_Base<T>::cur_size_=new_size;
	else if(new_size > this->max_size_)
	{
		T *tmp = new T[new_size]();

		for(size_t i = 0; i < this->Array_Base<T>::size(); ++i)
			tmp[i] = this->Array_Base<T>::get(i);

		if(this->Array_Base<T>::data_)
		{
			delete [] this->Array_Base<T>::data_;
			this->Array_Base<T>::data_ = NULL;
		}

		this->Array_Base<T>::data_ = new T[new_size];
		this->Array_Base<T>::cur_size_=new_size;
		this->max_size_ = new_size;
		
		for(size_t i = 0; i < new_size; ++i)
			this->Array_Base<T>::set(i, tmp[i]);
		
		if(tmp)
		{
			delete[] tmp;
			tmp = NULL;
		}
	}
}