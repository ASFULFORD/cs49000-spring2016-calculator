//==============================================================================
/**
 * Honor Pledge:
 *
 * I pledge that I have neither given nor received any help
 * on this assignment.
 */
//==============================================================================
#ifndef _EXPR_COMMAND_FACTORY_H_
#define _EXPR_COMMAND_FACTORY_H_

#include "Add_Command.h"
#include "Num_Op_Command.h"
#include "Subtract_Command.h"
#include "Multiply_Command.h"
#include "Divide_Command.h"
#include "Modulus_Command.h"

class Expr_Command_Factory
{
public:
	Expr_Command_Factory(void) { }
	virtual ~Expr_Command_Factory(void) { }

	virtual Num_Op_Command * create_number_command(int num) = 0;
	virtual Add_Command * create_add_command(void) = 0;
	virtual Subtract_Command * create_sub_command(void) = 0;
	virtual Multiply_Command * create_mult_command(void) = 0;
	virtual Divide_Command * create_div_command(void) = 0;
	virtual Modulus_Command * create_mod_command(void) = 0;
private:
	Expr_Command_Factory(Expr_Command_Factory &) { }
	Expr_Command_Factory & operator = (Expr_Command_Factory &) { }
};


#endif