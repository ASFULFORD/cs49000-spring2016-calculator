//==============================================================================
/**
 * Honor Pledge:
 *
 * I pledge that I have neither given nor received any help
 * on this assignment.
 */
//==============================================================================
#ifndef _STACK_EXPR_COMMAND_FACTORY_H_
#define _STACK_EXPR_COMMAND_FACTORY_H_

#include "Expr_Command_Factory.h"
#include "Num_Op_Command.h"
#include "Add_Command.h"
#include "Subtract_Command.h"
#include "Multiply_Command.h"
#include "Divide_Command.h"

class Stack_Expr_Command_Factory : public Expr_Command_Factory
{
public:
	Stack_Expr_Command_Factory(Stack <int> & s)
	: s_(s)
	{ }
	virtual ~Stack_Expr_Command_Factory(void) { }

	virtual Num_Op_Command * create_number_command(int num)
	{
		return new Num_Op_Command(s_, num);
	}

	virtual Add_Command * create_add_command(void)
	{
		return new Add_Command(s_);
	}

	virtual Subtract_Command * create_sub_command(void)
	{
		return new Subtract_Command(s_);
	}

	virtual Multiply_Command * create_mult_command(void)
	{
		return new Multiply_Command(s_);
	}

	virtual Divide_Command * create_div_command(void)
	{
		return new Divide_Command(s_);
	}

	virtual Modulus_Command * create_mod_command(void)
	{
		return new Modulus_Command(s_);
	}
private:
	Stack <int> & s_;

};


#endif