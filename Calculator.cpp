//==============================================================================
/**
 * Honor Pledge:
 *
 * I pledge that I have neither given nor received any help
 * on this assignment.
 */
//==============================================================================

#include "Calculator.h"
#include <sstream>
#include <iostream>
#include <string>

Calculator::Calculator(void)
: res_(-1),
  num_of_expr_(0),
  infix_(),
  postfix_(),
  result_(),
  factory_(result_)
{ }

Calculator::~Calculator(void) { }

int Calculator::get_result(void) 
{
	return 0;
}

std::string Calculator::get_infix(void)
{
	return this->infix_;
}

void Calculator::set_infix(std::string str)
{
	this->infix_ = str;
}

int Calculator::evaluate_postfix(void)
{
	for (size_t i = 0; i < this->num_of_expr_; ++i)
		postfix_.get(i)->execute();
	return this->result_.top();
}

int Calculator::string_to_int(std::string str)
{
	int number;
	if ( ! (std::istringstream(str) >> number) ) number = 0;
	return number;
}

void Calculator::infix_to_postfix(void)
{
	int inside_parenthesis_count = 0;
	int postfix_index = 0;
	this->num_of_expr_ = 0;
	int cmd_prec;
	int temp_prec;
	int parenthesis_temp_prec;
	std::istringstream input(this->infix_);
	std::string token;
	Expr_Command * cmd;
	Stack <Expr_Command *> temp;
	Stack <Expr_Command *> parenthesis_temp;
	while (!input.eof())
	{
		input >> token;
		if (token == "+")
		{
			cmd = this->factory_.create_add_command();
			this->num_of_expr_++;
			cmd_prec = cmd->get_precedence_val();
			if (!temp.is_empty())
				temp_prec = temp.top()->get_precedence_val();
			if (temp.is_empty() || cmd_prec > temp_prec)
				temp.push(cmd);
			else
			{
				while (!temp.is_empty() && cmd_prec <= temp_prec)
				{
					postfix_.set(postfix_index++, temp.top());
					temp.pop();
					if(!temp.is_empty())
						temp_prec = temp.top()->get_precedence_val();
				}
				temp.push(cmd);
			}
		}
		else if (token == "-")
		{
			cmd = this->factory_.create_sub_command();
			this->num_of_expr_++;
			cmd_prec = cmd->get_precedence_val();
			if (!temp.is_empty())
				temp_prec = temp.top()->get_precedence_val();
			if (temp.is_empty() || cmd_prec > temp_prec)
				temp.push(cmd);
			else
			{
				while (!temp.is_empty() && cmd_prec <= temp_prec)
				{
					postfix_.set(postfix_index++, temp.top());
					temp.pop();
					if(!temp.is_empty())
						temp_prec = temp.top()->get_precedence_val();
				}
				temp.push(cmd);
			}
		}
		else if (token == "*")
		{
			cmd = this->factory_.create_mult_command();
			this->num_of_expr_++;
			cmd_prec = cmd->get_precedence_val();
			if (!temp.is_empty())
				temp_prec = temp.top()->get_precedence_val();
			if (temp.is_empty() || cmd_prec > temp_prec)
				temp.push(cmd);
			else
			{
				while (!temp.is_empty() && cmd_prec <= temp_prec)
				{
					postfix_.set(postfix_index++, temp.top());
					temp.pop();
					if(!temp.is_empty())
						temp_prec = temp.top()->get_precedence_val();
				}
				temp.push(cmd);
			}
		}
		else if (token == "/")
		{
			cmd = this->factory_.create_div_command();
			this->num_of_expr_++;
			cmd_prec = cmd->get_precedence_val();
			if (!temp.is_empty())
				temp_prec = temp.top()->get_precedence_val();
			if (temp.is_empty() || cmd_prec > temp_prec)
				temp.push(cmd);
			else
			{
				while (!temp.is_empty() && cmd_prec <= temp_prec)
				{
					postfix_.set(postfix_index++, temp.top());
					temp.pop();
					if(!temp.is_empty())
						temp_prec = temp.top()->get_precedence_val();
				}
				temp.push(cmd);
			}
		}
		else if (token == "%")
		{
			cmd = this->factory_.create_mod_command();
			this->num_of_expr_++;
			cmd_prec = cmd->get_precedence_val();
			if (!temp.is_empty())
				temp_prec = temp.top()->get_precedence_val();
			if (temp.is_empty() || cmd_prec > temp_prec)
				temp.push(cmd);
			else
			{
				while (!temp.is_empty() && cmd_prec <= temp_prec)
				{
					postfix_.set(postfix_index++, temp.top());
					temp.pop();
					if(!temp.is_empty())
						temp_prec = temp.top()->get_precedence_val();
				}
				temp.push(cmd);
			}
		}
		else if (token >= "0" && token <= "9")
		{
			cmd = factory_.create_number_command(this->string_to_int(token));
			this->num_of_expr_++;
			postfix_.set(postfix_index++, cmd);
		}
		else if (token == "(")
		{
			while (token != ")")
			{
				input >> token;
				if (token == "+")
				{
					cmd = this->factory_.create_add_command();
					this->num_of_expr_++;
					inside_parenthesis_count++;
					cmd_prec = cmd->get_precedence_val();
					if (!parenthesis_temp.is_empty())
						parenthesis_temp_prec = parenthesis_temp.top()->get_precedence_val();
					if (parenthesis_temp.is_empty() || cmd_prec > parenthesis_temp_prec)
						parenthesis_temp.push(cmd);
					else
					{
						while (!parenthesis_temp.is_empty() && cmd_prec <= parenthesis_temp_prec)
						{
							postfix_.set(postfix_index++, parenthesis_temp.top());
							parenthesis_temp.pop();
							if(!parenthesis_temp.is_empty())
								parenthesis_temp_prec = parenthesis_temp.top()->get_precedence_val();
						}
						parenthesis_temp.push(cmd);
					}
				}
				else if (token == "-")
				{
					cmd = this->factory_.create_sub_command();
					this->num_of_expr_++;
					inside_parenthesis_count++;
					cmd_prec = cmd->get_precedence_val();
					if (!parenthesis_temp.is_empty())
						parenthesis_temp_prec = parenthesis_temp.top()->get_precedence_val();
					if (parenthesis_temp.is_empty() || cmd_prec > parenthesis_temp_prec)
						parenthesis_temp.push(cmd);
					else
					{
						while (!parenthesis_temp.is_empty() && cmd_prec <= parenthesis_temp_prec)
						{
							postfix_.set(postfix_index++, parenthesis_temp.top());
							parenthesis_temp.pop();
							if(!parenthesis_temp.is_empty())
								parenthesis_temp_prec = parenthesis_temp.top()->get_precedence_val();
						}
						parenthesis_temp.push(cmd);
					}
				}
				else if (token == "*")
				{
					cmd = this->factory_.create_mult_command();
					this->num_of_expr_++;
					inside_parenthesis_count++;
					cmd_prec = cmd->get_precedence_val();
					if (!parenthesis_temp.is_empty())
						parenthesis_temp_prec = parenthesis_temp.top()->get_precedence_val();
					if (parenthesis_temp.is_empty() || cmd_prec > parenthesis_temp_prec)
						parenthesis_temp.push(cmd);
					else
					{
						while (!parenthesis_temp.is_empty() && cmd_prec <= parenthesis_temp_prec)
						{
							postfix_.set(postfix_index++, parenthesis_temp.top());
							parenthesis_temp.pop();
							if(!parenthesis_temp.is_empty())
								parenthesis_temp_prec = parenthesis_temp.top()->get_precedence_val();
						}
						parenthesis_temp.push(cmd);
					}
				}
				else if (token == "/")
				{
					cmd = this->factory_.create_div_command();
					this->num_of_expr_++;
					inside_parenthesis_count++;
					cmd_prec = cmd->get_precedence_val();
					if (!parenthesis_temp.is_empty())
						parenthesis_temp_prec = parenthesis_temp.top()->get_precedence_val();
					if (parenthesis_temp.is_empty() || cmd_prec > parenthesis_temp_prec)
						parenthesis_temp.push(cmd);
					else
					{
						while (!parenthesis_temp.is_empty() && cmd_prec <= parenthesis_temp_prec)
						{
							postfix_.set(postfix_index++, parenthesis_temp.top());
							parenthesis_temp.pop();
							if(!parenthesis_temp.is_empty())
								parenthesis_temp_prec = parenthesis_temp.top()->get_precedence_val();
						}
						parenthesis_temp.push(cmd);
					}
				}
				else if (token == "%")
				{
					cmd = this->factory_.create_mod_command();
					this->num_of_expr_++;
					inside_parenthesis_count++;
					cmd_prec = cmd->get_precedence_val();
					if (!parenthesis_temp.is_empty())
						parenthesis_temp_prec = parenthesis_temp.top()->get_precedence_val();
					if (parenthesis_temp.is_empty() || cmd_prec > parenthesis_temp_prec)
						parenthesis_temp.push(cmd);
					else
					{
						while (!parenthesis_temp.is_empty() && cmd_prec <= parenthesis_temp_prec)
						{
							postfix_.set(postfix_index++, parenthesis_temp.top());
							parenthesis_temp.pop();
							if(!parenthesis_temp.is_empty())
								parenthesis_temp_prec = parenthesis_temp.top()->get_precedence_val();
						}
						parenthesis_temp.push(cmd);
					}
				}
				else if (token >= "0" && token <= "9")
				{
					cmd = factory_.create_number_command(this->string_to_int(token));
					this->num_of_expr_++;
					inside_parenthesis_count++;
					postfix_.set(postfix_index++, cmd);
				}
			}
			while(!parenthesis_temp.is_empty())
			{
				postfix_.set(postfix_index++, parenthesis_temp.top());
				parenthesis_temp.pop();
			}
		}
		else
		{
			std::cout << "Invalid Input" << std::endl;
			break;
		}
	}
	while(!temp.is_empty())
	{
		postfix_.set(postfix_index++, temp.top());
		temp.pop();
	}
	
}